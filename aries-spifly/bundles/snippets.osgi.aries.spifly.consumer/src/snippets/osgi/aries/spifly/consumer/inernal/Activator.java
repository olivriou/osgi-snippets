package snippets.osgi.aries.spifly.consumer.inernal;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.SwingUtilities;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import snippets.osgi.aries.spifly.api.IMyService;

public class Activator implements BundleActivator
{
	private final List<Runnable> stopActions = new ArrayList<>();

	@Override
	public void start(final BundleContext bundleContext) throws Exception
	{
		final DefaultListModel<String> dataModel = new DefaultListModel<>();

		final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		executor.scheduleAtFixedRate(() -> this.listServices(dataModel), 0, 1, TimeUnit.SECONDS);

		SwingUtilities.invokeLater(() -> {
			final JList<String> list = new JList<>(dataModel);
			list.setPreferredSize(new Dimension(300, 300));
			final JFrame f = new JFrame();
			f.getContentPane().add(list);
			f.pack();
			f.setAlwaysOnTop(true);
			f.setVisible(true);
			f.toFront();
			final WindowAdapter windowEvtHandler = new WindowAdapter()
			{

				@Override
				public void windowClosing(final WindowEvent e)
				{
					try
					{
						bundleContext.getBundle().stop();
					}
					catch (final BundleException e1)
					{
						e1.printStackTrace();
					}
				}
			};
			f.addWindowListener(windowEvtHandler);
			this.stopActions.add(() -> {
				f.removeWindowListener(windowEvtHandler);
				f.dispose();
			});
		});

		this.stopActions.add(executor::shutdownNow);
	}

	@Override
	public void stop(final BundleContext bundleContext) throws Exception
	{
		this.stopActions.forEach(Runnable::run);
	}

	private void listServices(final DefaultListModel<String> dataModel)
	{
		final List<String> services = new ArrayList<>();
		final Iterator<IMyService> ldr = ServiceLoader.load(IMyService.class).iterator();
		ldr.forEachRemaining(spiObject -> {
			services.add(spiObject.getInfo());
		});

		SwingUtilities.invokeLater(() -> {
			dataModel.clear();
			for (final String elt : services)
			{
				dataModel.addElement(elt);
			}

			if (dataModel.isEmpty())
			{
				dataModel.addElement("<No service found>");
			}
		});

	}

}
