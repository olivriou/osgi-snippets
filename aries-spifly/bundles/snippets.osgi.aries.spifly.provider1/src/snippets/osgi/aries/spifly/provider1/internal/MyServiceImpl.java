package snippets.osgi.aries.spifly.provider1.internal;

import snippets.osgi.aries.spifly.api.IMyService;

public class MyServiceImpl implements IMyService
{
	private static final String INFO = MyServiceImpl.class.getCanonicalName();

	@Override
	public String getInfo()
	{
		return INFO;
	}

}
